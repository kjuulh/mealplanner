import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './App.css';

import MenuBar from './components/Menubar';
import Home from './pages/Home';
import Login from './pages/Login';
import { rootReducer } from './store';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

const store = createStore(
  rootReducer,
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__(),
);

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Router>
        <MenuBar />
        <div className="App container">
          <Route path="/" exact component={Home} />
          <Route path="/login" component={Login} />
        </div>
      </Router>
    </Provider>
  );
};

export default App;
