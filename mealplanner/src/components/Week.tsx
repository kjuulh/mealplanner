import React, { Component } from 'react';
import WeekNumber from './WeekNumber';
import DaysInWeek from './DaysInWeek';

export default class Week extends Component {
  render() {
    return (
      <div>
        <WeekNumber />
        <DaysInWeek />
      </div>
    );
  }
}
