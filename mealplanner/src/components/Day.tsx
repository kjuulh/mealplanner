import React, { FunctionComponent } from 'react';
import Toggle from './Toggle';

type DayProps = {
  day: string;
  dinner: string;
};

export const Day: FunctionComponent<DayProps> = ({ day, dinner }) => {
  return (
    <tr>
      <th scope="row">{day}</th>
      <td>{dinner}</td>
      <td>
        <Toggle initialEnabled={false} />
      </td>
      <td>Enabled</td>
    </tr>
  );
};
