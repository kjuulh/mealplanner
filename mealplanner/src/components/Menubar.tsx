import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Menubar extends Component {
  render() {
    return (
      <nav className="navbar navbar-dark bg-dark justify-content-between">
        <Link to="/" className="navbar-brand">
          MEALPLANNER
        </Link>
        <Link to="/login" className="navbar-text">
          Login
        </Link>
      </nav>
    );
  }
}
