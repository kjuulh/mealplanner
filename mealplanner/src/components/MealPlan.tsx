import React, { FC } from 'react';
import { Day } from './Day';
import { AppState } from '../store';
import { useSelector } from 'react-redux';
import { MealPlanState } from '../store/mealplan/types';

const MealPlan: FC = () => {
  const mealPlan: MealPlanState = useSelector(
    (state: AppState) => state.mealPlan,
  );

  const daysList = mealPlan.days.map(({ weekday, dinner }, id) => (
    <Day key={id} day={weekday} dinner={dinner} />
  ));

  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">WEEKDAY</th>
            <th scope="col">DINNER</th>
            <th scope="col">ATTENDING</th>
            <th scope="col">ENABLED</th>
          </tr>
        </thead>
        <tbody>{daysList}</tbody>
      </table>
    </div>
  );
};

export default MealPlan;
