import React, { useState } from 'react';

type ToggleProps = {
  initialEnabled: boolean;
};

const Toggle = ({ initialEnabled }: ToggleProps) => {
  const [enabled, isEnabled] = useState(initialEnabled);

  return (
    <div>
      <button onClick={() => isEnabled(!enabled)}>
        {enabled ? 'Coming' : 'Not Coming'}
      </button>
    </div>
  );
};

export default Toggle;
