import React, { Component } from 'react';

import Week from '../components/Week';
import MealPlan from '../components/MealPlan';

export default class Home extends Component {
  render() {
    return (
      <div>
        <Week />
        <MealPlan />
      </div>
    );
  }
}
