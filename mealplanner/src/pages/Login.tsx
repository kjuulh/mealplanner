import React, { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOGIN, SystemState, LOGOUT } from '../store/system/types';
import { AppState } from '../store';

const Login: FC = () => {
  const system: SystemState = useSelector((state: AppState) => state.system);

  const [name, setName] = useState('');

  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (name !== '') {
      dispatch({ type: LOGIN, payload: { userName: name } });
    }

    console.log(system.userName);
  };

  const handleLogout = (event: any) => {
    event.preventDefault();
    dispatch({ type: LOGOUT });

    console.log(system.userName);
  };

  const dispatch = useDispatch();

  return (
    <div>
      <div>
        <h1>Login</h1>
      </div>
      <div>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label>
              <div>Name:</div>
            </label>
            <div>
              <input
                type="text"
                value={name}
                onChange={event => setName(event.target.value)}
                className="form-control"
                placeholder="Enter name"
              />
            </div>
          </div>
          <div>
            <span>
              <button onSubmit={handleSubmit} className="btn btn-primary">
                Login
              </button>
            </span>
            <span>
              <button onClick={handleLogout} className="btn btn-danger">
                Logout
              </button>
            </span>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
