export interface SystemState {
  loggedIn: boolean;
  userName: string;
}

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

interface LoginAction {
  type: typeof LOGIN;
  payload: SystemState;
}

interface LogoutAction {
  type: typeof LOGOUT;
  payload: SystemState;
}

export type SystemActionTypes = LoginAction | LogoutAction;
