import { SystemState, SystemActionTypes, LOGIN, LOGOUT } from './types';

export function login(loggedIn: SystemState): SystemActionTypes {
  return {
    type: LOGIN,
    payload: loggedIn,
  };
}

export function logout(loggedOut: SystemState): SystemActionTypes {
  return {
    type: LOGOUT,
    payload: loggedOut,
  };
}
