import { SystemState, SystemActionTypes, LOGIN, LOGOUT } from './types';

const initialState: SystemState = {
  loggedIn: false,
  userName: '',
};

export function systemReducer(
  state = initialState,
  action: SystemActionTypes,
): SystemState {
  switch (action.type) {
    case LOGIN:
      return {
        loggedIn: true,
        userName: action.payload.userName,
      };
    case LOGOUT:
      return {
        loggedIn: false,
        userName: '',
      };
    default:
      return state;
  }
}
