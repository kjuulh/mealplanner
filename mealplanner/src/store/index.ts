import { systemReducer } from './system/reducers';
import { mealPlanReducer } from './mealplan/reducers';

import { combineReducers } from 'redux';

export const rootReducer = combineReducers({
  system: systemReducer,
  mealPlan: mealPlanReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
