import { MealPlanState } from './types';

export const initialState: MealPlanState = {
  days: [
    {
      year: 1970,
      week: 1,
      weekday: 'monday',
      dinner: 'meatballs',
      user: '',
      attending: false,
    },
    {
      year: 1970,
      week: 1,
      weekday: 'tuesday',
      dinner: 'meatballs',
      user: '',
      attending: false,
    },
    {
      year: 1970,
      week: 1,
      weekday: 'wednesday',
      dinner: 'meatballs',
      user: '',
      attending: false,
    },
    {
      year: 1970,
      week: 1,
      weekday: 'thursday',
      dinner: 'meatballs',
      user: '',
      attending: false,
    },
    {
      year: 1970,
      week: 1,
      weekday: 'friday',
      dinner: 'meatballs',
      user: '',
      attending: false,
    },
    {
      year: 1970,
      week: 1,
      weekday: 'saturday',
      dinner: 'meatballs',
      user: '',
      attending: false,
    },
    {
      year: 1970,
      week: 1,
      weekday: 'sunday',
      dinner: 'meatballs',
      user: '',
      attending: false,
    },
  ],
};
