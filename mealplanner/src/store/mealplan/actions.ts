import {
  Day,
  MealPlanActionTypes,
  ATTEND_MEAL,
  DONT_ATTEND_MEAL,
} from './types';

export function attendMeal(notifyAttendMeal: Day): MealPlanActionTypes {
  return {
    type: ATTEND_MEAL,
    payload: notifyAttendMeal,
  };
}

export function dontAttendMeal(
  notifyNotAttendingMeal: Day,
): MealPlanActionTypes {
  return {
    type: DONT_ATTEND_MEAL,
    payload: notifyNotAttendingMeal,
  };
}
