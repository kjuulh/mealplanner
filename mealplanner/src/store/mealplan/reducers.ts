import {
  MealPlanState,
  MealPlanActionTypes,
  ATTEND_MEAL,
  DONT_ATTEND_MEAL,
} from './types';
import { initialState } from './initialState';

export function mealPlanReducer(
  state = initialState,
  action: MealPlanActionTypes,
): MealPlanState {
  switch (action.type) {
    case ATTEND_MEAL:
      return {
        days: state.days.map(day => {
          if (
            day.year === action.payload.year &&
            day.week === action.payload.week &&
            day.weekday === action.payload.weekday
          )
            return {
              ...day,
              attending: true,
            };
          else return day;
        }),
      };

    case DONT_ATTEND_MEAL:
      return {
        days: state.days.map(day => {
          if (
            day.year === action.payload.year &&
            day.week === action.payload.week &&
            day.weekday === action.payload.weekday
          )
            return {
              ...day,
              attending: false,
            };
          else return day;
        }),
      };

    default:
      return state;
  }
}
