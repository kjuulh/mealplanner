export interface Day {
  year: number;
  week: number;
  weekday: string;
  attending: boolean;
  dinner: string;
  user: string;
}

export interface MealPlanState {
  days: Day[];
}

export const ATTEND_MEAL = 'ATTEND_MEAL';
export const DONT_ATTEND_MEAL = 'DONT_ATTEND_MEAL';

interface AttendMealAction {
  type: typeof ATTEND_MEAL;
  payload: Day;
}

interface DontAttendMealAction {
  type: typeof DONT_ATTEND_MEAL;
  payload: Day;
}

export type MealPlanActionTypes = AttendMealAction | DontAttendMealAction;
