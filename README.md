# Meal Planner

Meal Planner is a practice application for getting familiar with React, Redux 
and friends, as such it isn't the best application, but I strive for it to provide
some value.

## Installation

Enter the folder and use your favorite npm package manager (I use yarn)

```bash
yarn install && yarn start
```

